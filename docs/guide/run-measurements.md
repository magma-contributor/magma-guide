# Performing network measurements

This section provides documentation and guidance specific to performing and
orchestrating network measurements.

## OONI

[OONI](network-measurements#ooni) develops software to perform network
measurement that detects Internet censorship or other forms of network
interfernce. The measurement tool that performs the internet measurements is the
OONI Probe client. The client runs in various OS types (Android, iOS, macOS and
Linux) and different user interfaces; command-line, Web and native graphical.
The clients have been developed in different programming languages Python with
the [Twisted networking framework](http://twistedmatrix.com/) is being used for
desktop, servers and embedded devices whereas the mobile platforms use the
portable C++11 network measurement library
[measurement-kit](https://github.com/measurement-kit/measurement-kit).

All officially supported software of OONI Probe along with installation
instructions can be found here:
[OONI Probe](https://github.com/ooni/probe#android-ios-desktop-cli)

### Available tests

A high level description of all OONI's software tests can be found here:
[OONI Nettest](https://ooni.io/nettest/). The detailed test specifications of
all tests as well as experiments tests can be found here:
[OONI Spec Nettests](https://github.com/ooni/spec/tree/master/nettests)

All OONI Probe software has different functionalities and testing capabilities.
The table below lists all the tests and their availability per software
platform:

| Test Name | Android | iOS | macOS | Linux |
|-----------|---------|-----|-------|-------|
| Bridge Reachability | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| Captive Portal | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| DASH | :white_check_mark: | :white_check_mark: | :negative_squared_cross_mark: | :negative_squared_cross_mark: |
| DNS Consistency | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| DNS Injection | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| DNS Spoof | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| Facebook Messenger | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| HTTP Header Field Manipulation | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| HTTP Host | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| HTTP Invalid Request Line | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| HTTP Requests | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| Lantern | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| Meek Fronted Requests | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| Multi Protocol Traceroute | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| NDT | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| OpenVPN | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| Psiphon | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| TCP Connect | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| Telegram | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| Vanilla Tor | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |
| Web Connectivity | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| WhatsApp | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| bridgeT | :negative_squared_cross_mark: | :negative_squared_cross_mark: | :white_check_mark: | :white_check_mark: |

### Helpers

#### OONI Run

In order to share custom OONI tests such as a Web Connectivity test with a list
of URLs you may use OONI Run, a website that generate a special type of link
that instructs OONI Probe to run specific tests (as defined by the OONI Run).

Complete instructions and an introductory post can be found here:
[https://ooni.org/post/ooni-run/](https://ooni.org/post/ooni-run/)

Please note that if you have a long list of URLs you can copy and paste them at
the first URL slot of OONI Run website. Note that the list of URLs should be
formatted as one URL per line:

```
https://www.example1.net
https://www.example2.org
```

Or simply with a space separator:

`https://www.example1.net https://www.example2.org`

Source: [OONI Run website](https://run.ooni.io/)

#### OONI Run (offline version)

The OONI Run link generator script (rungen.py) generates an OONI Run link
without the need to access an online website. It is also useful to
programmatically generate OONI Run links. This is the Python version of the
original [ooni-run
script](https://github.com/ooni/run/blob/master/utils/links.js)

It uses Python version 3.

Usage and instructions on how to run the script can be found here:
[OONI-rungen usage](https://github.com/azadi/ooni-rungen#usage)

Source: [ooni-rungen](https://github.com/azadi/ooni-rungen)

### Hardware-assisted network measurements

#### Lepidopter

Lepidopter is a Raspberry Pi distribution image with all the required
dependencies and software packages in place, configured to run network
measurement tests via the OONI Probe software. It is developed and designed to
require no physical attendance upon first bootstrap but also allows experienced
users to further configure it as they wish. The source code and the building
scripts of the Lepidopter image are free and open source software. Is is
currently based on the python (legacy) version of OONI Probe.

Lepidopter currently requireds the minimal physical attendance possible to
perform longitudinal, daily network measurements with OONI Probe.

Please read the complete installation instructions:
[Lepidopter Installation: Help Guides and Resources](https://ooni.torproject.org/install/lepidopter/)

Or simply download and copy the image to an SD card:
[Lepidopter releases](https://github.com/TheTorProject/lepidopter/releases)

### Risks

The OONI team has published extensive documentation on the risks associated with
the usage and submission of network measurements from OONI Probe. Please find
the detailed documentation in:
[Risks: Things you should know before using
ooniprobe](https://ooni.torproject.org/about/risks/)

### Opt-out

OONI Probe allows users to opt-out from having their data (network measurements)
published in OONI's database. Please review the complete list opt-out options
in: [OONI Data Policy](https://ooni.torproject.org/about/data-policy/)
